#include <iostream>
#include <ctime>
#include <string>
#include <vector>

using namespace std;

static const bool DEBUG = false;

class Board{
private:
    int SIZE;
    int num_turns; // for the randomizer
    bool won;
    vector< vector<int> > grid;
    
    // randomly place 2's
    void place2(bool fours) {
        srand(time(NULL));
        int i = rand() % SIZE;
        int j = rand() % SIZE;
        while(grid.at(i).at(j) != 0){
            i = rand() % SIZE;
            j = rand() % SIZE;
        }
        if (DEBUG){
            cout << "i = " << i << ", j = " << j << "\n\n";
        }
        if (fours && rand() % 10 == 1){
            grid.at(i).at(j) = 4;
        }
        else {
            grid.at(i).at(j) = 2;
        }
    }

    // calculate new grid based on input
    bool make_move(char input) {
        // make sure input is valid
        if (input != 'a' && input != 's' && input != 'd' && input != 'w') {
            return false;
        }
        bool has_changed = false;
        for (int i = 0; i < SIZE; i++){
            // create copy of row/col ordered left to right (for consistency)
            vector<int> copy(SIZE);
            switch(input) {
                case 'a': // left
                    copy = grid[i];
                    break;
                case 'd': // right
                    copy = grid[i];
                    reverse(copy.begin(), copy.end());
                    break;
                case 'w': // up
                    for (int j = 0; j < SIZE; ++j) {
                        copy[j] = grid[j][i];
                    }
                    break;
                case 's': // down
                    for (int j = 0; j < SIZE; ++j) {
                        copy[j] = grid[j][i];
                    }
                    reverse(copy.begin(), copy.end());
                    break;
            }
            // create replacement row
            vector<int> temp(SIZE);

            // fill in replacement row by analyzing data from copy row
            int current_pos = 0;
            for (int j = 0; j < SIZE; ++j){
                int val = copy[j];
                int temp_val = temp[current_pos];
                if (val != 0){
                    if (temp_val == 0) {
                        temp[current_pos] = val;
                        if (j != current_pos) {
                            has_changed = true;
                        }
                    }
                    else {
                        if (val == temp_val) {
                            temp[current_pos] = temp_val * 2;
                            if (temp_val == 1024) {
                                won = true;
                            }
                            ++current_pos;
                            has_changed = true;
                        }
                        else {
                            ++current_pos;
                            temp[current_pos] = val;
                        }
                    }
                }
            }

            // replace row/col!
            switch(input) {
                case 'a': // left
                    grid[i] = temp;
                    break;
                case 'd': // right
                    reverse(temp.begin(), temp.end());
                    grid[i] = temp;
                    break;
                case 'w': // up
                    for (int j = 0; j < SIZE; ++j) {
                        grid[j][i] = temp[j];
                    }
                    break;
                case 's': // down
                    reverse(temp.begin(), temp.end());
                    for (int j = 0; j < SIZE; ++j) {
                        grid[j][i] = temp[j];
                    }
                    break;
            }
        }
        return(has_changed);
    }

    // print the board
    void printBoard() {
        for (int i = 0; i < SIZE; ++i){
            cout << "______";
        }

        // top row
        cout << "_\n";

        // what
        for (int i = 0; i < SIZE; ++i){
           for (int j = 0; j < SIZE; ++j){
               cout << "|     ";
           }
           cout << "|\n";
           for (int j = 0; j < SIZE; ++j){
               if (grid.at(i).at(j) == 0){
                    cout << "|     ";
                }
                else if (grid[i][j] >= 1000){
                    cout << "| " << grid[i][j];
                }
                else if (grid[i][j] >= 100){
                    cout << "| " << grid[i][j] << " ";
                }
                else if (grid[i][j] >= 10){
                    cout << "|  " << grid[i][j] << " ";
                }
                else {
                    cout << "|  " << grid[i][j] << "  ";
                }
           }
           cout << "|\n";
           for (int j = 0; j < SIZE; ++j){
               cout << "|_____";
           }
           cout << "|\n";
       }
       cout << "\n\n\n";
    }
    
    // cin the move and do it
    void takeTurn() {
        char move;
        cout << "Enter a move (w/a/s/d) or press q to quit";

        //get input
        system("stty raw");
        move = getchar();
        system("stty cooked");

        bool has_changed = false;
        cout << "\n\n\n\n";
        switch (move) {
            case 'q':
                cout << "Thanks for playing!\n\n\n";
                exit(0);
            default:
                has_changed = make_move(move);
                break;
        }
        if (has_changed) {
            place2(true);
        }
        printBoard();
    }

    // returns whether a move can be made
    bool can_make_moves() {
        if (SIZE == 1) return false;
        for (int i = 0; i < SIZE; ++i){
            for (int j = 0; j < SIZE - 1; ++j){
                if (grid[i][j] == grid[i][j+1] || grid[i][j] == 0) return true;
            }
            if (grid[i][SIZE - 1] == 0) return true;
        }
        for (int j = 0; j < SIZE; ++j){
            for (int i = 0; i < SIZE - 1; ++i){
                if (grid[i][j] == grid[i+1][j] || grid[i][j] == 0) return true;
            }
            if (grid[SIZE - 1][j] == 0) return true;
        }
        return false;
    }

    // find max value
    int findMax() {
        int max = 0;
        for (int i = 0; i < SIZE; ++i){
            for (int j = 0; j < SIZE; ++j){
                if (grid[i][j] > max) max = grid[i][j];
            }
        }
        return max;
    }

public:
    // constructor
    Board(int size_in) {
        SIZE = size_in;
        num_turns = 0;
        won = false;
        vector<int> row;
        for (int i = 0; i < SIZE; ++i){
            row.push_back(0);
        }
        if (DEBUG){
            cout << "\nrows have been made\n\n";
        }
        for (int i = 0; i < SIZE; ++i){
            grid.push_back(row);
        }
        if (DEBUG){
            cout << "grids have been made\n\n";
        }
        place2(false);
        place2(false);

        if (DEBUG){
            cout << "numbers have been placed\n";
        }
        cout << "\n\n\n";
        printBoard();
    }

    // driver functions
    void play_game() {
        while (can_make_moves() && !won) takeTurn();
        // ended bc of 2048
        if (won){
            char keepPlaying;
            cout << "Would you like to keep playing? (y/n) \n";
            system("stty raw");
            keepPlaying = getchar();
            system("stty cooked");
            cout << "\n";
            if (keepPlaying == 'y') while (can_make_moves()) takeTurn();
        }
        // or ended bc no moves left
        if (!can_make_moves()){
            cout << "There are no moves left.\n\n";
        }
        cout << "Thanks for playing!\n\n";
    }

};

int main(){
    int len = 0;
    while (len < 2 || len > 10) {
        cout << "What size of board do you want?\n>> ";
        cin >> len;
        cout << len << "\n";
        if (cin.fail()) {
            cout << "Must enter a number.\n\n";
            exit(1);
        }
        else if (len < 2) {
            cout << "Size must be greater than 1.\n\n";
        }
        else if (len > 10) {
            cout << "Size must be 10 or less.\n\n";
        }
    }
    Board board(len);
    board.play_game();
}
